# Performance order

## Arvo
Including stormulator

## Spanish Trumpet 

## Trumpetulator
Kenneth solo

## Duet
Kenneth picks up Külli, goes through treeperc

## Külli Thinking Space

## Meil aia

## Percussive

## Earthulator
Through a very thin sound and gradual entry

## Underworld

## Byssan lull

## Bone Singing


# Light Cues

## Audience enters
- Audience lights
- Left side lights on 70%

## Show start
- Audience lights out
- Left side lights to 30% in 120 sec.

## Kenneth picks up microphone
- Go to general light in 120 sec.

## Bass drone begins, dancers in unison
- Contra lights to 50%
- Side lights to 30%
- Front lights to 20%

## Music goes to silence, dancers in front, looking at audience
- Go to general light in 20 sec.

## Earth quake music, dancers in a clump on the floor
- Go to top light only in 7 minutes.

## Külli returning from upside down cross
- Fade to black in 20 sec.



### general notes

