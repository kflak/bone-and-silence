BBSTrumpetPitchShifter {
    var db;
    var in;
    var out;
    var fadeInTime=1;
    var fadeOutTime=60;
    var fxChain;

    *new{|db=0, in=2, out=0|
        ^super.newCopyArgs(db, in, out).init;
    }

    init{
        this.prCreateFxChain;
    }

    prCreateFxChain{
        fxChain = FxChain.new(
            level: db.dbamp,
            out: out,
            in: in,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
        );
        fxChain.addPar(
            \dopplerPitchShift, [ \mix, 0.5, \ratio, -12.midiratio ],
            \dopplerPitchShift, [ \mix, 0.3, \ratio, 3.midiratio ],
            \dopplerPitchShift, [ \mix, 0.3, \ratio, 7.midiratio ],
        );
        fxChain.add(\flanger, [
            \feedback, 0.08,
            \depth, 0.04,
            \rate, 0.03,
            \decay, 0.01,
            \mix, 0.3,
        ]);
        fxChain.add(\jpverb,[
            \revtime, Pwhite(1.0, 4.0),
            \mix, Pwhite(0.3, 0.4),
        ]);
    }

    free {
        fxChain.free;
    }
}
