BBSWolfBreath {
    var <speed;
    var <out;
    var <fxChain;
    var <pbind;
    var <buffer;
    var <fadeInTime = 1;
    var <fadeOutTime = 3;
    var <db = 0;

    *new{|speed = 2|
        ^super.newCopyArgs(speed).init;
    }

    init {
        out = BBS.mainBus;
        this.prCreateFxChain;
        this.prCreatePbind;
        this.play;
    }

    db_{|val| 
        fxChain.level = val.dbamp;
    }

    prCreateFxChain {
        fxChain = FxChain.new(
            level: db.dbamp,
            out: out,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
            numInputBusChannels: 2, 
        );

        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, 0.2
        ]);
    }

    prCreatePbind{
        buffer = BBS.buf[\wolfBreath][0];
        pbind = PbindProxy(
            \instrument, \grain,
            \buf, buffer,
            \numFrames, buffer.numFrames,
            \bufDur, buffer.duration,
            \jitter, 100,
            \startPos, Pn(
                Pfintime(
                    buffer.duration,
                    Pseg([0, Pkey(\numFrames) - Pkey(\jitter) - 1], Pkey(\bufDur)) + Pwhite(0, Pkey(\jitter)), 
                    ),
                inf
            ),
            \dur, 0.1,
            \attack, 0.1,
            \decay, 0.1,
            \db, Pseg(Pwhite(-70, -20), Pwhite(10, 20), repeats: inf),
            \out, fxChain.in,
            \group, fxChain.group,
        );
    }

    play {
        fxChain.play;
        pbind.play;
    }

    free{
        fork{
            fxChain.free;
            (fadeOutTime - 0.5).wait;
            pbind.clear;
        }
    }
}
