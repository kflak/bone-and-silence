BBSMicSwing {

    var in;
    var <db;
    var fadeInTime;
    var fadeOutTime;
    var fxChain;

    *new {
        arg in=2, db= 0, fadeInTime=20, fadeOutTime=5;
        ^super.newCopyArgs(in, db, fadeInTime, fadeOutTime).init;
    }

    db_{|val| fxChain.db = val; db = val}
    init {
        if (in.isNil){
            in = BBS.numSpeakers + BBS.numSubs;
        };
        fxChain = FxChain.new(
            in: in,
            fadeInTime: fadeInTime,
            level: db.dbamp,
            fadeOutTime: fadeOutTime,
            out: BBS.mainBus,
            numInputBusChannels: 1,
        ); 

        fxChain.add(\panner, [
            \pan, 0,
        ]);

        fxChain.add(\ringMod, [
            \mix, Pseg([1.0, 1.0, 0.0], [110, 60]),
            \modfreq, 2,
            \lag, 1,
            \amp, 3.dbamp,
            \depth, 3,
        ]);

        fxChain.addPar(
            \comb, [ \delay, 0.4, \decay, 2, \mix, Pseg([0.0, 0.0, 0.7, 0.7, 0.0], [40, 20, 50, 20]) ],
            \comb, [ \delay, 0.7, \decay, 2, \mix, Pseg([0.0, 0.0, 0.7, 0.7, 0.0], [40, 20, 50, 20]) ],
            \comb, [ \delay, 0.9, \decay, 2, \mix, Pseg([0.0, 0.0, 0.7, 0.7, 0.0], [40, 20, 50, 20]) ], 
        );

        fxChain.addPar(
            \comb, [ \amp, -3.dbamp, \delay, 57.midicps.reciprocal, \decay, 2, \mix, Pseg([0.0, 0.0, 0.4, 0.4, 0.0], [70, 20, 15, 25]) ],
            \comb, [ \amp, -3.dbamp, \delay, 60.midicps.reciprocal, \decay, 2, \mix, Pseg([0.0, 0.0, 0.4, 0.4, 0.0], [70, 20, 15, 25]) ],
            \comb, [ \amp, -3.dbamp, \delay, 62.midicps.reciprocal, \decay, 2, \mix, Pseg([0.0, 0.0, 0.4, 0.4, 0.0], [70, 20, 15, 25]) ], 
        );

        fxChain.add(\jpverb, [
            \mix, 0.3,
        ]);

        fxChain.add(\eq, [
            \locut, 80,
        ]); 
    }

    free {
        fxChain.free;
    }
}
