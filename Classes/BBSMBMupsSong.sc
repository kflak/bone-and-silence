BBSMBMupsSong : BBSMBShuffle {

    *new{|db= -24, speedlim=0.5, threshold=0.02, minAmp= -14, maxAmp=6, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initBBSMBMupsSong;
    }

    initBBSMBMupsSong {
        this.prAddFx;
        buf = BBS.buf[\mupssongFull];
        grainSize = 0.2;
        minRate = 0.99;
        maxRate = 1.01;
        freeMBsImmediately = false;
    }

    prAddFx {
        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, Pwhite(0.01, 0.3), Pwhite(1, 5),
            ]); 
        fxChain.add(\greyhole, [
            \delayTime, 0.3,
            \feedback, 0.4,
            \mix, Pwhite(0.01, 0.3), Pwhite(1, 5),
            ]); 
        fxChain.add(\eq, [
            \locut, 80,
            ]);
    }
}
