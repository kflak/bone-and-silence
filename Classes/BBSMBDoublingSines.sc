BBSMBDoublingSines : BBSMBDeltaTrig {
    var low, high;

    *new{|db= -18, speedlim=0.5, threshold=0.05, minAmp= -18, maxAmp= -6, fadeInTime=30, fadeOutTime=5|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initBBSMBDoublingSines;
    }

    initBBSMBDoublingSines {
        this.prAddFx;
        low = Pseg([0, -24], 180).asStream;
        high = Pseg([48, 12], 180).asStream;
    }

    prAddFx {
        fxChain.add(\jpverb, [
            \revtime, 1,
            \mix, 0.4,
        ])
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var l = low.next;
            var h = high.next;
            Pbind(
                \instrument, \sine,
                \attack, 0,
                \degree, Pwhite(l, h),
                \dur, Pwhite(1/16, 1/8),
                \release, 0.2,
                \legato, 0.1,
                \pan, Pwhite(0.0, 1.0, dt.linlin(0.0, 1.0, 3, 8)),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
