BBSTester.new;

BBSMBShuffleTester.new;

BBS.speakerTest;

BBS.numSpeakers = 2;
b = BBS.new;

d = BBSMBGrainDelay.new(
    maxDelay: 10,
    rates: #[1, 1.5, 2, 3],
);
d.play(mbs: #[9, 15]);
d.play(mbs: #[9]);
d.freeMB(15);
d.freeMB(9);
d.free(fadeOutTime: 10);

t = BBSMBTrumpetGrainDelay.new;
t.play;
t.freeMB(15);
t.free;
