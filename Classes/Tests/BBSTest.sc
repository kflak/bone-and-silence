BBSTest1 : UnitTest {
	test_check_classname {
		var result = BBS.new;
		this.assert(result.class == BBS);
	}
}

BBSSetNumInputBusChannelsTest : UnitTest {
    var b;
    setUp{
        b = BBS.new;
    }
	test_set_num_inputbuses {
        var b = BBS.new;
		this.assert(Server.default.options.numInputBusChannels == 8);
	} }

BBSSetNumOutputBusChannelsTest : UnitTest {
    var b;
    setUp {
       BBS.numSpeakers = 4;
       BBS.numSubs = 1;
       b = BBS.new;
    }

	test_set_num_outputbuses {
		var result = Server.default.options.numOutputBusChannels;
		this.assert(result == 5);
        postf("numOutputBusChannels %\n", result);
	}
}

BBSSetLatencyTest : UnitTest {
	test_set_latency {
		var result = Server.default.latency;
		this.assert(result == 0.1);
	}
}

BBSBootServerTest : UnitTest {
    test_boot_server {
        Server.default.sync;
        this.assert(Server.default.serverRunning);
    }
}

BBSSynthDefTest : UnitTest {
    test_synth_defs {
        var result;
        Server.default.sync;
        result = SynthDescLib.getLib(\global).at(\sine).isNil;
        this.assert(result == false);
    }
}

BBSTester {
	*new {
		^super.new.init();
	}

	init {
		BBSTest1.run;
        BBSSetNumInputBusChannelsTest.run;
        BBSSetNumOutputBusChannelsTest.run;
        BBSSetLatencyTest.run;
        BBSBootServerTest.run;
        BBSSynthDefTest.run;
	}
}
