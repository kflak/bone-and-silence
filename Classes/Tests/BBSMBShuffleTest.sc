BBSMBShuffleIsClassTest : UnitTest{
	test_check_classname {
		var result = BBSMBShuffle.new;
		this.assert(result.class == BBSMBShuffle);
	}
}

BBSMBShuffleCreateMBDeltaTrigTest : UnitTest {
    test_create_mbdeltatrig {
        var b = BBSMBShuffle.new;
        var result = b.mbDeltaTrigs;
        this.assert(result.isCollection && result[0].class == MBDeltaTrig);
    }
}

BBSMBShuffleSetInitTimeTest : UnitTest {
    test_set_init_time {
        var b = BBSMBShuffle.new;
        var result = b.initTime;
        this.assert(result == thisThread.seconds);
    }
}

BBSMBShuffleTester {
	*new {
		^super.new.init();
	}

	init {
        BBSMBShuffleIsClassTest.run;
        BBSMBShuffleCreateMBDeltaTrigTest.run;
        BBSMBShuffleSetInitTimeTest.run;
	}
}
