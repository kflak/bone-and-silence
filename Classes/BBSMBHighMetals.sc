BBSMBHighMetals : BBSMBDeltaTrig {
    var <>rateArray = #[1, -0.5, 0.25];
    var <distMix = 0.0;
    var <reverbMix = 0.4;
    var <>filterMB = 9;
    var <>minFilterFreq = 4000;
    var <>maxFilterFreq = 15000;
    var currentPos = 0;
    
    *new{|db=0, speedlim=0.5, threshold=0.03, minAmp= -10, maxAmp= 0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initBBSMBHighMetals;
    }

    initBBSMBHighMetals{
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\eq, [
            \locut, Pfunc{ BBS.mbData[filterMB].x.linexp(0.0, 1.0, minFilterFreq, maxFilterFreq) }, 0.1,
            \hishelffreq, 400,
            \hishelfdb, -6,
            \lag, 0.5,
        ]);
        fxChain.add(\comb, [
            \delay, Pwhite(0.002, 0.004),
            \decay, 1,
            \lag, 1,
            \mix, Pfunc{ BBS.mbData[filterMB].delta }, 0.05,
        ]);
        fxChain.add(\jpverb, [
            \lag, 5,
            \revtime, 3,
            \mix, reverbMix,
        ]);
        fxChain.add(\eq, [
            \hishelffreq, 600,
            \hishelfdb, -3,
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|

            var buf = BBS.buf[\verkstedshallen][0];
            var numFrames = buf.numFrames;
            var dur = 0.2;
            var step = dur * BBS.server.sampleRate;
            var len = dt.linlin(0.0, 1.0, step, step * 20);
            var pos = (currentPos, currentPos+step..currentPos+len);
            currentPos = pos[pos.size-1].mod(numFrames);

            Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, dur,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \startPos, Pseq(pos),
                \legato, 2,
                \rate, Pfunc{ BBS.mbData[filterMB].x.linexp(0.0, 1.0, 2.0, 16.0)},
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }

    // distMix_{|val| fxChain.fx[\noiseMod].set(\mix, val); distMix = val}
    reverbMix_{|val| fxChain.fx[\jpverb].set(\mix, val); reverbMix = val}
}
