BBSMBByssanlull : BBSMBShuffle {

    *new{|db= -42, speedlim=0.3, threshold=0.01, minAmp= -6, maxAmp=0, fadeInTime=1, fadeOutTime=60|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initBBSMBByssanlull;
    }

    initBBSMBByssanlull {
        this.prAddFx;
        buf = BBS.buf[\byssanlull];
        grainSize = 0.4;
        freeMBsImmediately = false;
        loop = true;
    }

    prAddFx {

        fxChain.add(\jpverb, [
            \revtime, Pwhite(0.1, 4), Pwhite(1, 5),
            \mix, 0.3,
            ]); 
        fxChain.add(\eq, [
            \lag, 1,
            \locut, Pseg(Pwhite(80, 200), Pwhite(1, 4)),
            \hicut, Pseg(Pwhite(200, 1600), Pwhite(1, 4)),
            ]);
    }
}
