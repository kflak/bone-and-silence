(
var synthDefAsrWithPan = (
    pink: {|sig|
        var lag = \lag.kr(0.1);
        sig = PinkNoise.ar(); 
        sig = BHiPass.ar(sig, \locut.kr(20, lag));
        sig = BLowPass.ar(sig, \hicut.kr(20000, lag));
        sig = sig * LFNoise1.ar(\lfnoiseFreq.kr(10));
    },

    playbuf: {|sig|
        var buf = \buf.kr(0);
        sig = PlayBuf.ar(
            1, 
            buf, 
            rate: BufRateScale.kr(buf) * \rate.kr(1), 
            startPos: \startPos.kr(0),
            loop: \loop.kr(1)
        );
    },

    membrane: {|sig|
        var excitation = EnvGen.ar(Env.perc(0.01, 1, 1), doneAction: 0) * PinkNoise.ar(0.4);
        sig = MembraneCircle.ar(
            excitation,
            \tension.kr(0.05),
            \loss.kr(0.99999)
        );
    },

    klang: {|sig|
        var freq = \freq.kr(440);
        var freqs = Array.geom(8, freq, \spread.ir(2));
        var amps = Array.geom(8, 1, \decay.ir(0.3));
        sig = Klang.ar(`[freqs, amps, nil]);
        sig = sig * AmpCompA.kr(freq);
    },

    sine: {|sig|
        var freq = \freq.kr(440, 0.1);
        sig = SinOsc.ar(freq);
        sig = sig * AmpCompA.kr(freq);
    },

    analogBassDrum: {|sig|
        var gate = \gate.kr(1);
        sig = AnalogBassDrum.ar(
            gate,
            \infsustain.kr(0.0),
            \accent.kr(0.5),
            \freq.kr(50),
            \tone.kr(0.5)
        );
    },

    analogSnareDrum: {|sig|
        var gate = \gate.kr(1);
        sig = AnalogSnareDrum.ar(
            gate,
            \infsustain.kr(0.0),
            \accent.kr(0.5),
            \freq.kr(200),
            \tone.kr(0.5),
            \decay.kr(0.5),
            \snappy.kr(0.5)
        );
    },

    pmFB: {|sig|
        var lag = \lag.kr(0.1);
        var freq = \freq.kr(440, lag);
        var modenv = EnvGen.kr(Env.asr(\modattack.kr(0.1), 1.0, \modrelease.kr(0.1)), gate: \gate.kr(1));
        var mod = modenv * \pmindex.kr(0, lag);
        sig = PMOscFB.ar(
            freq,
            \modfreq.kr(440, lag),
            mod,
            \modfeedback.kr(0.0, lag)
        );
        sig = sig * AmpCompA.kr(freq);
    },

    vosc: {|sig|
        var freq = \freq.kr(440);
        sig  = VOsc.ar(
            \bufpos.kr(0), 
            freq
        );
        sig = sig * AmpCompA.kr(freq);
    },

    rec: {|sig|
        var in = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs));
        var id = \id.ir(0);
        var rec = \rec.ar(1);
        var buf = \buf.kr(0);
        var frames = BufFrames.kr(buf);
        var phase = Phasor.ar(0, BufRateScale.kr(buf), 0, frames);
        var stopTrig = (rec <= 0);
        BufWr.ar(in, buf, phase);
        SendReply.ar(Impulse.ar(\trigFreq.kr(20)), '/recPos', phase, id);
        SendReply.ar(stopTrig, '/recEnded', phase, id);
        sig = in * \amp.kr(1);
    },

    panner: {|sig|
        sig = In.ar(\in.kr(BBS.numSpeakers), 1);
    },

);

var synthDefAsrWithoutPan = (
    lfnoise: {|sig|
    var lfnoise = LFNoise1.ar(\freq.kr(10)) ! BBS.numSpeakers;
    sig = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
    sig = sig * lfnoise;
    },

    gain: {|sig|
        sig = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
        sig = sig * \gain.kr(1);
    },

    dist: {|sig|
        sig = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
        sig = sig * \gain.kr(1);
        sig = sig.distort;
    },

    eq: {|sig|
        var lag = \lag.kr(0.1);
        sig = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
        sig = BLowShelf.ar(sig, \loshelffreq.kr(200, lag), db: \loshelfdb.kr(0, lag));
        sig = BHiShelf.ar(sig, \hishelffreq.kr(1500, lag), db: \hishelfdb.kr(0, lag));
        sig = BHiPass.ar(sig, \locut.kr(20, lag));
        sig = BLowPass.ar(sig, \hicut.kr(20000, lag));
        sig = BPeakEQ.ar(sig, \peakfreq.kr(600, lag), \peakrq.kr(1, lag), \peakdb.kr(0, lag));
    },

    greyhole: {|sig|
        var source, mix, lag;
        lag = \lag.kr(0.1);
        mix = \mix.kr(0.3, lag);
        source = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
        source = Splay.ar(source);
        sig = Greyhole.ar(
            source, 
            \delayTime.kr(1, lag),
            \damp.kr(0, lag),
            \size.kr(1, lag),
            \diff.kr(0.707, lag),
            \feedback.kr(0.9), 
            \modDepth.kr(0.1), 
            \modFreq.kr(2.0)
        );
        sig = Splay.ar(sig, spread: 0.5);
        sig = (1 - mix) * source + (sig * mix);
    },

    // jpverb: {|sig|
    //     var source, mix, lag;
    //     lag = \lag.kr(0.1);
    //     mix = \mix.kr(0.3, lag);
    //     source = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
    //     source = Mix.ar(source);
    //     sig = JPverb.ar(
    //         source,
    //         \revtime.kr(1), 
    //         \damp.kr(0), 
    //         \size.kr(1), 
    //         \early.kr(0.707)
    //     );
    //     sig = SplayAz.ar(BBS.numSpeakers, sig, spread: 0.5, width:BBS.numSpeakers);
    //     sig = (1 - mix) * source + (sig * mix);
    // },

    route: {|sig|
        sig = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
    },

    dopplerPitchShift: {|sig|
        var lag = \lag.kr(0.1);
        var mix = \mix.kr(1.0, lag);
        var source = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
        // source = Mix.ar(source);
        sig = DopplerPitchShift.ar(source, \ratio.kr(1));
        sig = (1 - mix) * source + (sig * mix);
    },

    flanger: {|sig|
        var in, maxdelay, maxrate, dsig, mixed, mix, local, lag; 
        lag = \lag.kr(0.1);
        mix = \mix.kr(1, lag);
        maxdelay = 0.013; 
        maxrate = 10.0; 
        in = In.ar(\in.kr((BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers)); 
        local = LocalIn.ar(BBS.numSpeakers); 
        dsig = AllpassL.ar(
            in + (local * \feedback.kr(0.0, lag)), 
            maxdelay * 2, 
            LFPar.kr( 
                \rate.kr(0.06, lag) * maxrate, 
                0, 
                \depth.kr(0.08, lag) * maxdelay, 
                \delay.kr(0.1, lag) * maxdelay
            ), 
            \decay.kr(0.0, lag)
        ); 
        mixed = in + dsig;
        LocalOut.ar(mixed); 
        sig = (1 - mix) * in + (mixed * mix);
    },

    ringMod: {|sig|
        var source, mix, mod, lag;
        lag = \lag.kr(0.1);
        mix = \mix.kr(1, lag);
        mod = SinOsc.ar(\modfreq.kr(1, lag)) * \depth.kr(1, lag);
        source = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
        sig = source * mod;
        sig = (1 - mix) * source + (sig * mix);
    },

    comb: {|sig|
        var source, mix, lag;
        lag = \lag.kr(0.1);
        source = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
        sig = CombC.ar(
            source, 
            \maxDelay.ir(1),
            \delay.kr(0.1, lag),
            \decay.kr(1, lag)
        );
        mix = \mix.kr(1, lag);
        sig = (1 - mix) * source + (sig * mix);
    },

    compressor: {|sig|
        var lag = \lag.kr(0.1);
        sig = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
        sig = Compander.ar(
            sig,
            sig, 
            slopeAbove: \ratio.kr(2, lag).reciprocal, 
            thresh: \thresh.kr(0.7, lag), 
            clampTime: \clampTime.kr(0.1, lag)
        );
    },
    noiseMod: {|sig|
        var lag = \lag.kr(0.1);
        var mod = LFNoise1.ar(\modfreq.kr(1, lag)) * \depth.kr(1, lag);
        var source = In.ar(\in.kr(BBS.numSpeakers+BBS.numSubs), BBS.numSpeakers);
        var mix = \mix.kr(1, lag);
        sig = source * mod;
        sig = (1 - mix) * source + (sig * mix);
    },

    limiter: {|sig|
        var lag = \lag.kr(0.1);
        sig = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
        sig = Limiter.ar(sig, \limit.kr(0.9, lag));
    },

    granulator: {|sig|
        var buf = \buf.kr(0);
        var tFreq, rate, posRate;
        var phasor;
        var bufFrames = BufFrames.ir(buf); 
        var t;
        var lag = \lag.kr(0.05);
        var tFreqMod = { 
            SinOsc.ar(\tFreqModFreq.kr(0), Rand(0.0,2pi)) * \tFreqModDepth.kr(0);
        };
        var rateMod = { 
            SinOsc.ar(\rateModFreq.kr(0), Rand(0.0,2pi)) * \rateModDepth.kr(0);
        };
        var posRateMod = { 
            SinOsc.ar(\posRateModFreq.kr(0), Rand(0.0,2pi)) * \posRateModDepth.kr(0);
        };

        tFreq = \tFreq.kr(20, lag) + tFreqMod ! BBS.numSpeakers;
        posRate = \posRate.kr(1) + posRateMod ! BBS.numSpeakers;
        rate = \rate.kr(1) + rateMod ! BBS.numSpeakers;

        t = Impulse.ar(tFreq);

        phasor = Phasor.ar(
            rate: posRate * BufRateScale.kr(buf), 
            start: 0.0, 
            end: bufFrames, 
        );

        sig = GrainBuf.ar(
            numChannels: 1, 
            trigger: t, 
            dur: tFreq.reciprocal * \overlap.kr(2), 
            sndbuf: buf, 
            rate: rate, 
            pos: phasor / bufFrames, 
        );
    },

    grbufphasor: {|sig|

        var pos, posDev, buf, duration, trig, rate, rateDev, startPos, endPos, pan, panDev, playbackRate;
        buf = \buf.kr(0);
        playbackRate = \playbackRate.kr(1);
        startPos = \startPos.kr(0);
        endPos = \endPos.kr(1);
        duration = (endPos - startPos).abs * (BufFrames.kr(buf) / SampleRate.ir());
        trig = Impulse.kr(\grainfreq.kr(10));
        posDev = \posDev.kr(0);
        pos = Line.kr(startPos, endPos, (duration * playbackRate.reciprocal)); 
        pos = (pos + TRand.kr(0.0, posDev, trig)).mod(endPos);
        rate = \rate.kr(1);
        rateDev =  \rateDev.kr(0);
        rate = rate + TRand.kr(0.0, rateDev, trig);
        pan = \pan.kr(0);
        panDev = \panDev.kr(0);
        pan = pan + TRand.kr(0.0, panDev, trig);
        // pan = pan.mod(1.0);
        sig = GrainBuf.ar(BBS.numSpeakers, trig, \grainsize.kr(0.2), buf, rate, pos, pan: pan);
    }
);

var synthDefPercWithPan = (
    grain: {|sig|
        var buf = \buf.kr(0);
        sig = PlayBuf.ar(1, buf, rate: BufRateScale.kr(buf) * \rate.kr(1), startPos: \startPos.kr(0), loop: \loop.kr(1));
    },

    playbufPerc: {|sig|
        var buf = \buf.kr(0);
        sig = PlayBuf.ar(
            1, 
            buf, 
            rate: BufRateScale.kr(buf) * \rate.kr(1), 
            startPos: \startPos.kr(0),
            loop: \loop.kr(0)
        );
    },
);

var synthDefPercWithoutPan = (
    pinkPerc: {|sig|
        var lag = \lag.kr(0.1);
        sig = PinkNoise.ar(); 
        sig = BHiPass.ar(sig, \locut.kr(20, lag));
        sig = BLowPass.ar(sig, \hicut.kr(20000, lag));
        sig = sig * LFNoise1.ar(10);
    }
);

var jpverb;

if(BBS.numSpeakers==2){
    jpverb = {|sig|
        var source, mix, lag;
        lag = \lag.kr(0.1);
        mix = \mix.kr(0.3, lag);
        source = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
        // source = Mix.ar(source);
        sig = JPverb.ar(
            source,
            \revtime.kr(1),
            \damp.kr(0),
            \size.kr(1),
            \early.kr(0.707)
        );
        // sig = SplayAz.ar(BBS.numSpeakers, sig, spread: 0.5, width:BBS.numSpeakers);
        sig = (1 - mix) * source + (sig * mix);
    };
};
if(BBS.numSpeakers==4){
    jpverb = {|sig|
        var source, mix, lag, rev1, rev2;
        lag = \lag.kr(0.1);
        mix = \mix.kr(0.3, lag);
        source = In.ar(\in.kr(BBS.numSpeakers + BBS.numSubs), BBS.numSpeakers);
        // source = Mix.ar(source);
        rev1 = JPverb.ar(
            source[0..1],
            \revtime.kr(1),
            \damp.kr(0),
            \size.kr(1),
            \early.kr(0.707)
        );
        rev2 = JPverb.ar(
            source[2..3],
            \revtime.kr(1),
            \damp.kr(0),
            \size.kr(1),
            \early.kr(0.707)
        );
        sig = rev1 ++ rev2;
        // sig = SplayAz.ar(BBS.numSpeakers, sig, spread: 0.5, width:BBS.numSpeakers);
        sig = (1 - mix) * source + (sig * mix);
    };
};

synthDefPercWithPan.keysValuesDo{|name, func|
    MakeSynthDef(
        name, 
        func, 
        env: \perc, 
        pan: true, 
        numOutputChannels: BBS.numSpeakers
    );
};

synthDefPercWithoutPan.keysValuesDo{|name, func|
    MakeSynthDef(
        name, 
        func, 
        env: \perc, 
        pan: false, 
        numOutputChannels: BBS.numSpeakers
    );
};

synthDefAsrWithPan.keysValuesDo{|name, func|
    MakeSynthDef(
        name, 
        func, 
        env: \asr, 
        pan: true, 
        numOutputChannels: BBS.numSpeakers
    );
};

synthDefAsrWithoutPan.keysValuesDo{|name, func|
    MakeSynthDef(
        name, 
        func, 
        env: \asr, 
        pan: false, 
        numOutputChannels: BBS.numSpeakers
    );
};

MakeSynthDef(
    \jpverb,
    jpverb,
    env: \asr,
    pan: false,
    numOutputChannels: BBS.numSpeakers
)

)
